import urllib
import json
import time
import datetime
import sys
import os

config = json.load(open('config.json'))

RED   = "\033[1;31m"
GREEN = "\033[0;32m"


def check_state(config):
    out = {}
    usdsum = 0

    for coin in config['coins']:
        url = "https://api.coinmarketcap.com/v1/ticker/" + coin['coin']
        response = urllib.urlopen(url)
        data = json.loads(response.read())[0]

        networth = float(coin['amount']) * float(data['price_usd'])
        usdsum += networth

        out[data['name']] = {
            "price": {
                "usd": float(data['price_usd']),
            },
            "market cap usd": data['market_cap_usd'],
            "change (%)": {
                "hourly": data['percent_change_1h'],
                "daily": data['percent_change_24h'],
                "weekly": data['percent_change_7d']
            },
            "money": {
                "in": coin['usd_in'] if 'usd_in' in coin else '',
                "balance": networth,
                "profit": networth - coin['usd_in'] if 'usd_in' in coin else ''
            },
            "AM I RICH YET": "YES :)" if networth > int(config["richness_level_usd"]) else "NO :("
        }

    if usdsum > config['richness_level_usd']:
        sys.stdout.write(GREEN)
    else:
        sys.stdout.write(RED)

    return out


if config["refresh_rate"]:
    while True:
        os.system('clear')
        d = check_state(config)
        print "\n==============================================================="
        print datetime.datetime.now().strftime("%d-%m-%Y %H:%M")
        print "---------------------------------------------------------------"
        print json.dumps(d, sort_keys=True, indent=4).replace("{", "").replace("}", "")
        print "===============================================================\n"
        time.sleep(int(config['refresh_rate']))




