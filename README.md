## Config options

* __refresh_rate__ refresh interval in seconds
* __richness_level_usd__ amount of USD needed to consider yourself rich
* __coins__ - list of coins to be tracked
    * __coin__ - name of the coin
    * __amount__ - amount of coins you own
    * __usd_in__ - amount of USD you spent on coin

